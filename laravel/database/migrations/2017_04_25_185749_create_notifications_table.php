<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    public function up()
    {
        Schema::create('notifications', function(Blueprint $table)
        {
            $table->increments('id');

            $table->enum('type', ['upload', 'collection', 'follow']);

            $table->string('image');
            $table->text('content');
            $table->string('link');

            $table->boolean('read');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('notifications');
    }
}
