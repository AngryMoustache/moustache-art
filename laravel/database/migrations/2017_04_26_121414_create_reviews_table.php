<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    public function up()
    {
        Schema::create('reviews', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('score');
            $table->text('review');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('upload_id')->unsigned()->index();
            $table->foreign('upload_id')->references('id')->on('uploads')->onDelete('cascade');

            $table->timestamps();
            $table->softdeletes();
        });
    }

    public function down()
    {
        Schema::drop('reviews');
    }
}
