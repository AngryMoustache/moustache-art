<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionUploadTable extends Migration
{
    public function up()
    {
        Schema::create('collection_upload', function(Blueprint $table)
        {
            $table->integer('collection_id')->unsigned()->index();
            $table->foreign('collection_id')->references('id')->on('collections')->onDelete('cascade');
            $table->integer('upload_id')->unsigned()->index();
            $table->foreign('upload_id')->references('id')->on('uploads')->onDelete('cascade');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    public function down()
    {
        Schema::drop('collection_upload');
    }
}
