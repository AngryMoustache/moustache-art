<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowsTable extends Migration
{
    public function up()
    {
        Schema::create('follows', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('target_id')->unsigned()->index();
            $table->foreign('target_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
            $table->softdeletes();
        });
    }

    public function down()
    {
        Schema::drop('follows');
    }
}
