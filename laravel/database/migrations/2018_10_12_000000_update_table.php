<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTable extends Migration
{
    public function up()
    {
        Schema::table('collection_upload', function ($table) {
            $table->timestamps();
            $table->softdeletes();
        });
    }

    public function down()
    {
        Schema::drop('collection_upload');
    }
}
