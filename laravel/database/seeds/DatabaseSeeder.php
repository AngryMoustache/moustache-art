<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Clotion",
            'password' => "6c14c5eae3a5354f48dc3834862b8aee",
            'email' => "private_clotion@hotmail.com",
            'avatar' => "tumblr_mr81ldZKSR1ro3irro1_400.jpg",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('follows')->insert([
            'user_id' => 1,
            'target_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('uploads')->insert([
            'name' => "Test",
            'image' => "62552672c0871fd44943df8525a55531.png",
            'artist' => "ArtistName",
            'description' => "Description for the upload",
            'private' => 0,
            'user_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);  

        DB::table('tags')->insert([ 'name' => "upload" ]);
        DB::table('tags')->insert([ 'name' => "test" ]);

        DB::table('upload_user')->insert([
            'upload_id' => 1,
            'user_id' => 1
        ]);

        DB::table('upload_tag')->insert([
            'upload_id' => 1,
            'tag_id' => 1
        ]);

        DB::table('upload_tag')->insert([
            'upload_id' => 1,
            'tag_id' => 2
        ]);

        DB::table('collections')->insert([
            'name' => "Favorites",
            'description' => "",
            'private' => 0,
            'user_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('collection_upload')->insert([
            'collection_id' => 1,
            'upload_id' => 1
        ]);
    }
}
