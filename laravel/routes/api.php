<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['cors']], function ($router) {

    Route::resource('/users', 'UsersController');
    Route::resource('/uploads', 'UploadsController');
    Route::resource('/collections', 'CollectionsController');
    Route::resource('/tags', 'TagsController');
    Route::resource('/notifications', 'NotificationsController');
    Route::resource('/follows', 'FollowsController');
    Route::resource('/reviews', 'ReviewsController');

    Route::post('/uploads/edit/{id}', 'UploadsController@update');

    Route::get('/follows/{user_id}/{upload_id}', 'FollowsController@showFollowing');
    Route::get('/notifications/user/{user}/full', 'NotificationsController@userfull');
    Route::get('/notifications/user/{user}', 'NotificationsController@user');
    Route::post('/notifications/read', 'NotificationsController@read');

    Route::post('/users/avatar', 'UsersController@uploadAvatar');
    Route::get('/uploads/tag/{tag}', 'UploadsController@indexTag');
    Route::post('/users/login', 'UsersController@login');
    Route::post('/users/register', 'UsersController@register');
    Route::post('/collections/addtocollections', 'CollectionsController@addCollectionUpload');
    Route::post('/collections/removefromcollections', 'CollectionsController@removeCollectionUpload');

    Route::resource('/pivot/collection_upload', 'CollectionUploadController');

});

