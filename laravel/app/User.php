<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    public function collections()
    {
        return $this->hasMany('App\Collection');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
