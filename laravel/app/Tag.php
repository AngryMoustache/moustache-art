<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Tag extends Authenticatable
{
    use Notifiable;

    public function uploads()
    {
        return $this->belongsToMany('App\Upload', 'upload_tag');
    }
}
