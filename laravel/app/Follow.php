<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Follow extends Authenticatable
{
    use Notifiable;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function target()
    {
        return $this->belongsTo('App\User', 'target_id');
    }
}
