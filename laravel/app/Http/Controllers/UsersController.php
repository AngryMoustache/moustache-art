<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Collection};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class UsersController extends Controller
{
    public $tablename = 'users';

    public function create()
    {
        //
    }

    public function login(Request $request)
    {
        $user = User::where('name', '=', Input::get('username'))->get();
        if (COUNT($user) != 0)
        {
            if ($user[0]->password == md5($request->password))
            {
                return [200, $user];
            }

            return 'That is not the correct password.';
        }

        return 'That username could not be found.';
    }

    public function register(Request $request)
    {
        $user = User::where('name', '=', Input::get('username'))->get();
        if (COUNT($user) == 0)
        {
            if ($request->password === $request->password_2)
            {
                $user = User::where('email', '=', Input::get('email'))->get();
                if (COUNT($user) == 0)
                {
                    if (strpos($request->username, ' ') !== false)
                    {
                        return 'Spaces are not allowed in your name.';
                    }
                    else
                    {
                        $uploaddata = new User;
                        $uploaddata->name      = $request->username;
                        $uploaddata->password  = md5($request->password);
                        $uploaddata->email     = $request->email;
                        $uploaddata->avatar    = 'empty_avatar.png';
                        $uploaddata->save();

                        // Make the Favorite collection
                        $favoritesdata = new Collection;
                        $favoritesdata->name = 'Favorites';
                        $favoritesdata->description = '';
                        $favoritesdata->private = 0;
                        $favoritesdata->user_id = $uploaddata->id;
                        $favoritesdata->created_at  = Carbon::now()->format('Y-m-d H:i:s');
                        $favoritesdata->save();

                        return [200, User::where('name', '=', $request->username)->get()];
                    }
                }
                else
                {
                    return 'That email is already in use!';
                }
            }
            else
            {
                return 'Those passwords do not match.';
            }            
        }
        else
        {
            return 'A user with that name already exists.';
        }
    }

    public function store(Request $request)
    {
        //
    }

    public function show($name)
    {
        $uploads = User::where('users.deleted_at', null)
            ->where('users.name', '=', $name)
            ->with(['uploads' => function ($query) {
                $query->orderBy('uploads.id', 'DESC');
                $query->where('uploads.deleted_at', null);
            }])
            ->get();
        return $uploads;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function uploadAvatar(Request $request)
    {
        // Save image
        $file = $request->file('image');
        $destinationPath = 'gallery/avatars';
        if ($file->isValid())
        {
            $filename = md5(str_replace(' ', '_', $file->getFileName() . rand())) . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);

            User::where('id', $request->id)->update(['avatar' => $filename]);
            return User::where('id', $request->id)->get();
        }
        else
        {
            return 0;
        }
    }
}
