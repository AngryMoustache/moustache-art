<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $tablename;

    public function index()
    {
        $data = collect(DB::table($this->tablename)->where('deleted_at', null)->get());
        return $data;
    }

    public function destroy($id)
    {
        DB::select('UPDATE uploads SET deleted_at = "' . Carbon::now() . '" WHERE id = ' . $id);
        return 200;
    }
}
