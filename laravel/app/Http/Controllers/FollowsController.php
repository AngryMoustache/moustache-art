<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Notification, Follow};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class FollowsController extends Controller
{
    public function index()
    {
        $data = Follow::with('target')->where('deleted_at', '=', null)->with('user')->get();
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // Follow or unfollow?
        $amount = DB::table('follows')
            ->where('user_id', '=', $request->user_id)
            ->where('target_id', '=', $request->target_id)
            ->where('deleted_at', '=', null)
            ->get();

        if (COUNT($amount) == 0)
        {
            // Follow
            $data = new Follow;
            $data->user_id     = $request->user_id;
            $data->target_id   = $request->target_id;
            $data->created_at  = Carbon::now()->format('Y-m-d H:i:s');
            $data->save();
        }
        else
        {
            // Unfollow
            Follow::where('user_id', '=', $request->user_id)->where('target_id', '=', $request->target_id)->update(['deleted_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        }

        // Make a notification
        if (COUNT(DB::table('notifications')->where('content', '=', $request->username . ' has followed you.')->get()) == 0)
        {
            DB::table('notifications')->insert([
                'type' => 'follow',
                'image' => $request->image,
                'content' => $request->username . ' has followed you.',
                'link' => $request->username,
                'read' => false,
                'user_id' => $request->target_id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        return COUNT($amount);
    }

    public function show($id)
    {
        $data = Follow::with('target')->where('user_id', '=', $id)->where('deleted_at', '=', null)->with('user')->get();
        return $data;
    }

    public function showFollowing($user_id, $target_id)
    {
        $data = Follow::with('target')
            ->where('user_id', '=', $user_id)
            ->where('target_id', '=', $target_id)
            ->where('deleted_at', '=', null)
            ->with('user')->get();
        return COUNT($data);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
