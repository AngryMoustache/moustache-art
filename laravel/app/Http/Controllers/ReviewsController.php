<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Collection, Review};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class ReviewsController extends Controller
{
    public $tablename = 'reviews';

    public function index()
    {
        $data = Review::where('reviews.deleted_at', null)->get();

        return $data;
    }

    public function store(Request $request)
    {
    	$data = new Review;
        $data->score     = $request->score;
        $data->review    = Input::get('review');
        $data->user_id   = $request->user_id;
        $data->upload_id = $request->upload_id;
        $data->save();

        return $data;
    }
}
