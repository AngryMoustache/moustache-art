<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class TagsController extends Controller
{
    public function index()
    {
        $data = Tag::where('tags.deleted_at', '=', null)->get();
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
