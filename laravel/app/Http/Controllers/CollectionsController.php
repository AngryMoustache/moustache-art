<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Collection, Notification};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class CollectionsController extends Controller
{
    public function index()
    {
        $data = Collection::where('collections.deleted_at', '=', null)
            ->join('users', 'collections.user_id', '=', 'users.id')
            ->with(['uploads' => function ($query) {
                $query->orderBy('uploads.id', 'DESC');
                $query->where('uploads.deleted_at', null);
            }])
            ->select(
                'collections.id',
                'collections.name',
                'collections.description',
                'collections.deleted_at',
                'collections.private',
                'users.name AS creator'
            )
            ->orderBy('collections.id', 'DESC')
            ->get();
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $uploaddata = new Collection;
        $uploaddata->name        = $request->name;
        if ($request->description == null)
        {
            $request->description = '';
        }
        $uploaddata->description = $request->description;
        $uploaddata->private     = $request->private;
        $uploaddata->user_id     = User::where('name', '=', $request->username)->pluck('id')[0];
        $uploaddata->save();

        return 200;
    }

    public function addCollectionUpload(Request $request)
    {
        DB::table('collection_upload')->insert([
            'collection_id' => $request->collection_id,
            'upload_id' => $request->upload_id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        // Make a notification
        DB::table('notifications')->insert([
            'type' => 'collection',
            'image' => $request->image,
            'content' => $request->username . ' has added ' . $request->uploadname . ' to his/her ' . Collection::where('id', '=', $request->collection_id)->pluck('name')[0],
            'link' => $request->username,
            'read' => false,
            'user_id' => User::where('name', '=', $request->targetname)->pluck('id')[0],
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return 200;
    }

    public function removeCollectionUpload(Request $request)
    {
        DB::delete('DELETE FROM collection_upload WHERE collection_id = ' . $request->collection_id . ' AND upload_id = ' . $request->upload_id);
        return 200;
    }

    public function show($name)
    {
        $data = Collection::where('collections.deleted_at', null)
            ->join('users', 'collections.user_id', 'users.id')
            ->where('users.name', '=', $name)
            ->with(['uploads' => function ($query) {
                $query->orderBy('collection_upload.created_at', 'DESC');
                $query->where('uploads.deleted_at', null);
                // $query->where('uploads.user_id', 'creator_id');
            }])
            ->select(
                'collections.id',
                'collections.name',
                'collections.description',
                'collections.deleted_at',
                'collections.private',
                'users.id AS creator_id',
                'users.name AS creator'
            )
            ->get();

        $creator_id = $data[0]->creator_id;
        $_data = [];

        for ($i = 0; $i < COUNT($data); $i++)
        // foreach ($data as $i => $uploads)
        {
            $_data[$i] = new Collection;
            $_data[$i]->id = $data[$i]->id;
            $_data[$i]->name = $data[$i]->name;
            $_data[$i]->description = $data[$i]->description;
            $_data[$i]->private = $data[$i]->private;
            $_data[$i]->creator = $data[$i]->creator;

            $_uploads = [];

            foreach ($data[$i]->uploads as $upload)
            // foreach ($uploads->uploads as &$upload)
            {
                if ($upload->user_id == $creator_id)
                {
                    $_uploads[] = $upload;
                }
            }

            $_data[$i]->uploads = $_uploads;
        }

        return $_data;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        DB::select('UPDATE collections SET deleted_at = "' . Carbon::now() . '" WHERE id = ' . $id);
        return 200;
    }
}
