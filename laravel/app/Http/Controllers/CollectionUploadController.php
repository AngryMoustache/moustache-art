<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Collection};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class CollectionUploadController extends Controller
{
    public function index()
    {
        $data = DB::table('collection_upload')->get();
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
