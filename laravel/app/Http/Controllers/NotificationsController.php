<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Notification};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class NotificationsController extends Controller
{
    public function index()
    {
        $data = Notification::get();
        return $data;
    }

    public function user($id)
    {
        $data = Notification::where('user_id', '=', $id)->where('read', '=', false)->orderBy('created_at', 'DESC')->count();
        return $data;
    }

    public function userfull($id)
    {
        $data = Notification::where('user_id', '=', $id)->orderBy('created_at', 'DESC')->get();
        return $data;
    }

    public function read(Request $request)
    {
        Notification::where('id', $request->id)->update(['read' => 1]);
        return Notification::where('user_id', '=', $request->user_id)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
