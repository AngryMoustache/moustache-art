<?php

namespace App\Http\Controllers;

use DB;
use App\{Tag, Upload, User, Follow, Notification};
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Validator, Input, Redirect};

class UploadsController extends Controller
{
    public function index()
    {
        $data = Upload::with('tags')
            ->join('users', 'uploads.user_id', '=', 'users.id')
            ->where('uploads.deleted_at', '=', null)
            ->select(
                'uploads.id',
                'uploads.name',
                'uploads.image',
                'uploads.artist',
                'uploads.private',
                'uploads.description',
                'uploads.deleted_at',
                'users.name AS uploader'
            )
            ->orderBy('uploads.id', 'DESC')
            ->get();
        return $data;
    }

    public function indexTag($tag)
    {
        $data = Tag::with('uploads')
            ->where('tags.name', '=', $tag)
            ->get();
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user = User::where('name', '=', Input::get('uploader'))->pluck('id')[0];

        // Save image
        $file = $request->file('image');
        $destinationPath = 'gallery';
        if ($file->isValid())
        {
            $picNumber = Upload::count();
            $filename = md5(str_replace(' ', '_', $file->getFileName() . '_' . $picNumber)) . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename );
        }
        else
        {
            return 0;
        }
        
        // Store
        $uploaddata = new Upload;
        $uploaddata->name = Input::get('name');

        if (null !== Input::get('artist')) $uploaddata->artist = Input::get('artist');
        else $uploaddata->artist = '';

        if (null !== Input::get('description')) $uploaddata->description  = Input::get('description');
        else $uploaddata->description  = 'No description given.';

        $uploaddata->image   = $filename;
        $uploaddata->user_id = $user;
        $uploaddata->private = Input::get('private');
        $uploaddata->save();
        
        for ($i = 0; $i < COUNT(explode(' ', Input::get('tags'))); $i++)
        {
            $tag = Tag::where('name', '=', explode(' ', strtolower(Input::get('tags')))[$i] )->first();

            if ($tag === null)
            {
                $data = new Tag;
                $data->name = strtolower(explode(' ', Input::get('tags'))[$i]);
                $data->save();
        
                DB::table('upload_tag')->insert([
                    'upload_id' => $uploaddata->id,
                    'tag_id' => $data->id
                ]);
            }
            else
            {
                DB::table('upload_tag')->insert([
                    'upload_id' => $uploaddata->id,
                    'tag_id' => $tag->id
                ]);
            }

            DB::table('upload_user')->insert([
                'upload_id' => $uploaddata->id,
                'user_id' => $user
            ]);
        }

        // Notifications, but not if private
        if (Input::get('private') == 0)
        {
            $targets = Follow::with('target')->where('target_id', '=', $user)->where('deleted_at', '=', null)->get();
            foreach ($targets as $target)
            {
                DB::table('notifications')->insert([
                    'user_id' => $target->user_id,
                    'type' => 'upload',
                    'image' => $filename,
                    'link' => str_replace(" ", "-", $filename) . '-' . $uploaddata->id,
                    'content' => $target->target->name . ' has uploaded ' . Input::get('name') . '.',
                    'read' => false,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
        }
        
        return Upload::where('id', '=', $uploaddata->id)->select('id', 'name')->get();
    }

    public function show($id)
    {
        $reviewsscore = -1;

        $data = Upload::with('tags')
            ->join('users', 'uploads.user_id', '=', 'users.id')
            ->with(['reviews' => function ($query) {
                $query->join('users', 'reviews.user_id', '=', 'users.id')->select(
                    'reviews.*',
                    'users.name',
                    'users.avatar',
                    'users.id AS userId')
                ->get();
            }])
            ->where('uploads.deleted_at', '=', null)
            ->where('uploads.id', '=', $id)
            ->select(
                'uploads.id',
                'uploads.name',
                'uploads.image',
                'uploads.description',
                'uploads.private',
                'uploads.artist',
                'users.avatar',
                'users.name AS uploader')
            ->get();

        // $data[0]->reviewsscore = $reviewsscore;
        $data[0]->description = nl2br($data[0]->description);
        return $data;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $artist = Input::get('artist');
        if (Input::get('artist') == null) $artist = '';

        // Store
        DB::table('uploads')
            ->where('id', $id)
            ->update(array(
                'name' => Input::get('name'),
                'artist' => $artist,
                'description' => Input::get('description'),
                'private' => Input::get('private')
            )
        );

        # Get rid of all tags_uploads
        DB::select('DELETE FROM upload_tag WHERE upload_id = ' . $id);
        
        for ($i = 0; $i < COUNT(explode(' ', Input::get('tags'))); $i++)
        {
            $tag = Tag::where('name', '=', explode(' ', strtolower(Input::get('tags')))[$i] )->first();

            if ($tag === null)
            {
                $data = new Tag;
                $data->name = strtolower(explode(' ', Input::get('tags'))[$i]);
                $data->save();
        
                DB::table('upload_tag')->insert([
                    'upload_id' => $id,
                    'tag_id' => $data->id
                ]);
            }
            else
            {
                DB::table('upload_tag')->insert([
                    'upload_id' => $id,
                    'tag_id' => $tag->id
                ]);
            }
        }
        
        return Upload::where('id', '=', $id)->select('id', 'name')->get();
    }
}
