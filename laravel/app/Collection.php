<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Collection extends Authenticatable
{
    use Notifiable;

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload', 'collection_upload');
    }
}
