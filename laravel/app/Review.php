<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Review extends Authenticatable
{
    use Notifiable;

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    public function user()
    {
        return $this->hasOne('App\User')->select(array('name'));
    }
}
