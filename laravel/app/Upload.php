<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Upload extends Authenticatable
{
    use Notifiable;

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'upload_tag');
    }

    public function collections()
    {
        return $this->belongsToMany('App\Collection', 'collection_upload');
    }
}
