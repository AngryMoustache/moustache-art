import './bootstrap';
import router from './routes';
import App from './App.vue';

new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },
    router
});
