import VueRouter from 'vue-router';
import App from './App.vue';
import Home from './components/views/Home.vue';
import View from './components/views/View.vue';
import Tags from './components/views/Tags.vue';
import Upload from './components/views/Upload.vue';
import User from './components/views/User.vue';
import Collection from './components/views/Collection.vue';
import Notifications from './components/views/Notifications.vue';

let routes = [
    { path: '/', component: Home },
    { path: '/view/:id', component: View },
    { path: '/tags/:tag', component: Tags },
    { path: '/upload', component: Upload },
    { path: '/notifications', component: Notifications },
    { path: '/user/:name', component: User },
    { path: '/collection/:name', component: Collection },
];

export default new VueRouter({
    routes
});
