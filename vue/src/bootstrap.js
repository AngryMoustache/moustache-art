import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'vue-awesome/icons'; // The icons
import Icon from 'vue-awesome/components/Icon.vue';

window.Vue = Vue;
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'Content-Type, Authorization, Content-Length, X-Requested-With'
};

window.apiurl = "http://private-app.local";

Vue.use(VueRouter);
Vue.component('icon', Icon);
